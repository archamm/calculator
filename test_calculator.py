import unittest
from calculator import add, subtract, multiply, divide, power_of

class TestCalculator(unittest.TestCase):

    def test_add(self): 
        self.assertEqual(add(1, 2), 3)
        
    def test_subtract(self):
        self.assertEqual(subtract(3, 2), 1)
        
    def test_multiply(self):
        self.assertEqual(multiply(2, 3), 6)
        
    def test_divide(self):
        self.assertEqual(divide(6, 3), 2)
        with self.assertRaises(ValueError):
            divide(1, 0)
    def test_power_of(self):
        self.assertEqual(power_of(3, 2), 9)



