Feature: Calculator operations

  Scenario: performing multiple operations
     Given I start with 5
     When I add 5
     And I subtract 2
     And I multiply by 3
     And I divide by 2
     Then I should get 14
