from behave import *
from calculator import add, subtract, multiply, divide

@given('I start with {num:d}')
def step_start_with(context, num):
    context.result = num

@when('I add {num:d}')
def step_add(context, num):
    context.result = add(context.result, num)

@when('I subtract {num:d}')
def step_subtract(context, num):
    context.result = subtract(context.result, num)

@when('I multiply by {num:d}')
def step_multiply(context, num):
    context.result = multiply(context.result, num)

@when('I divide by {num:d}')
def step_divide(context, num):
    context.result = divide(context.result, num)

@then('I should get {expected:d}')
def step_check_result(context, expected):
    assert context.result == expected