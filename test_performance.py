import pytest
from calculator import add

def test_addition_benchmark(benchmark):
    result = benchmark(add, 1, 2)
    assert result == 3
