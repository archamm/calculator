pipeline {
    agent any 

    stages {
        stage('Setup') {
            steps {
                script {
                    // Create a virtual environment
                    sh '''
                    python3 -m venv venv
                    '''

                    // Activate the virtual environment and install dependencies
                    sh '''
                    . venv/bin/activate
                    pip3 install -r requirements.txt
                    '''
                }
            }
        }

        stage('Linting') {
            steps {
                script {
                    // Activate the virtual environment and run Pylint
                    sh '''
                    . venv/bin/activate
                    python3 -m pylint --output-format=parseable *.py > pylint-report.txt || true
                    cat pylint-report.txt
                    '''
                }
                recordIssues tools: [pyLint(pattern: 'pylint-report.txt')]
            }
        }

        stage('Unit Tests') {
            steps {
                script {
                    // Activate the virtual environment and run unit tests
                    sh '''
                    . venv/bin/activate
                    python3 -m unittest test_calculator.py
                    '''
                }
            }
        }

        stage('Integration Tests') {
            steps {
                script {
                    // Activate the virtual environment and run integration tests
                    sh '''
                    . venv/bin/activate
                    behave
                    '''
                }
            }
        }

        stage('Performance Tests') {
            steps {
                    sh '''
                    . venv/bin/activate
                    pytest --benchmark-only test_performance.py
                    '''
            }
        }
    }

    post {
        always {
            // Delete the virtual environment after all stages are completed
            sh 'rm -rf venv'
        }
    }
}
